package ricardo.teammanager.daos;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;

import ricardo.teammanager.models.Member;
import ricardo.teammanager.utils.HibernateUtil;

public class MemberDao {

	private MemberDao() {

	}

	public static Member find(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		session.beginTransaction();

		Member member = session.get(Member.class, id);

		session.close();

		return member != null ? member.clone() : null;

	}

	public static List<Member> search(String code, String name, int offset, int limit) {
		limit = limit <= 0 ? 10 : limit;

		offset = offset <= 0 ? 0 : offset;

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		session.beginTransaction();
		
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		
		AbstractQuery<Member> abstractQuery = criteriaBuilder.createQuery(Member.class);  

		Root<Member> root = abstractQuery.from(Member.class);
		
		if(!name.isEmpty() && !name.isBlank()) {
			abstractQuery.where(criteriaBuilder.like(root.get("name"), "%" + name + "%"));
		}
		
		if(!code.isEmpty() && !code.isBlank()) {
			abstractQuery.where(criteriaBuilder.equal(root.get("code"), code));
		}
		
		CriteriaQuery<Member> criteriaQuery = ((CriteriaQuery<Member>)abstractQuery).select(root);
		
		TypedQuery<Member> typedQuery = session.createQuery(criteriaQuery)
				.setFirstResult(offset)
				.setMaxResults(limit);
		
		List<Member> members = typedQuery.getResultList();

		session.close();

		return members;
	}

	public static List<Member> list(int offset, int limit) {
		limit = limit <= 0 ? 10 : limit;

		offset = offset <= 0 ? 0 : offset;

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		session.beginTransaction();

		List<Member> members = session.createQuery("FROM Member", Member.class).setFirstResult(offset)
				.setMaxResults(limit).list();

		session.close();

		return members;

	}

	public static void create(Member member) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		session.beginTransaction();

		member = (Member) member.clone();

		session.save(member);

		session.close();

	}

	public static void update(Member member) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		Transaction transaction = session.beginTransaction();

		member = member.clone();

		session.update(member);

		transaction.commit();

		session.close();

	}

	public static void delete(Member member) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		Transaction transaction = session.beginTransaction();

		member = member.clone();

		session.delete(member);

		transaction.commit();

		session.close();

	}

}
