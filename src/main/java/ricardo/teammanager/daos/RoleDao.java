package ricardo.teammanager.daos;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ricardo.teammanager.models.Role;
import ricardo.teammanager.utils.HibernateUtil;

public class RoleDao {

	private RoleDao() {

	}
	
	public static Role find(int id) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		session.beginTransaction();

		Role role = session.get(Role.class, id);;

		session.close();

		return role != null ? role.clone() : null;

	}
	
	public static List<Role> search(String name, int offset, int limit) {
		limit = limit <= 0 ? 10 : limit;

		offset = offset <= 0 ? 0 : offset;

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		session.beginTransaction();
		
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		
		AbstractQuery<Role> abstractQuery = criteriaBuilder.createQuery(Role.class);  

		Root<Role> root = abstractQuery.from(Role.class);
		
		if(!name.isEmpty() && !name.isBlank()) {
			abstractQuery.where(criteriaBuilder.like(root.get("name"), "%" + name + "%"));
		}
		
		CriteriaQuery<Role> criteriaQuery = ((CriteriaQuery<Role>)abstractQuery).select(root);
		
		TypedQuery<Role> typedQuery = session.createQuery(criteriaQuery)
				.setFirstResult(offset)
				.setMaxResults(limit);
		
		List<Role> roles = typedQuery.getResultList();

		session.close();

		return roles;
	}

	public static List<Role> list(int limit, int offset) {

		limit = limit <= 0 ? 1 : limit;

		offset = offset <= 0 ? 10 : offset;

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		session.beginTransaction();

		List<Role> roles = session.createQuery("FROM Role", Role.class).setFirstResult(offset).setMaxResults(limit)
				.list();

		session.close();

		return roles;

	}

	public static void create(Role role) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		session.beginTransaction();

		role = role.clone();

		session.save(role);

		session.close();

	}

	public static void update(Role role) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		Transaction transaction = session.beginTransaction();

		role = role.clone();

		session.update(role);
		
		transaction.commit();

		session.close();

	}

	public static void delete(Role role) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		Transaction transaction = session.beginTransaction();

		role = role.clone();

		session.delete(role);
		
		transaction.commit();

		session.close();

	}
}
