package ricardo.teammanager.daos;

import java.util.List;

import org.hibernate.Session;

import ricardo.teammanager.models.Entry;
import ricardo.teammanager.utils.HibernateUtil;

public class EntryDao {

	private EntryDao() {

	}

	public static Entry find(int id) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		session.beginTransaction();

		Entry entry = session.get(Entry.class, id);

		session.close();

		return entry != null ? entry.clone() : null;

	}

	public static List<Entry> list(int limit, int offset) {

		limit = limit <= 0 ? 1 : limit;

		offset = offset <= 0 ? 10 : offset;

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		session.beginTransaction();

		List<Entry> entries = session.createQuery("FROM Entry", Entry.class).setFirstResult(offset).setMaxResults(limit)
				.list();

		session.close();

		return entries;

	}

	public static void create(Entry entry) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		session.beginTransaction();

		entry = (Entry) entry.clone();

		session.save(entry);

		session.close();

	}

	public static void update(Entry entry) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		session.beginTransaction();

		entry = (Entry) entry.clone();

		session.saveOrUpdate(entry);

		session.close();

	}

	public static void delete(Entry entry) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		session.beginTransaction();

		entry = (Entry) entry.clone();

		session.delete(entry);

		session.close();

	}
}
