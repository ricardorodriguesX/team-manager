package ricardo.teammanager.utils.validator.validations;

import ricardo.teammanager.utils.validator.Validation;
import ricardo.teammanager.utils.validator.ValidationResult;

public class Required extends Validation{

	public Required(Object value) 
	{
		super(value);
	}

	@Override
	public ValidationResult validate() { 
		
		if(this.getValue() == null)
		{
			return new ValidationResult(-1, "Field is null");
		}
		
		if(this.getValue().toString().isBlank() || this.getValue().toString().isEmpty())
		{
			return new ValidationResult(-2, "Field is empty");
		}
		
		return new ValidationResult(0, "Valid");
	}

}
