package ricardo.teammanager.utils.validator.validations;

import ricardo.teammanager.utils.validator.Validation;
import ricardo.teammanager.utils.validator.ValidationResult;

public class IsNumber extends Validation{

	public IsNumber(Object value) 
	{
		super(value);
	}
	
	@Override
	public ValidationResult validate() { 
		
		if(this.getValue() == null)
		{
			return new ValidationResult(-1, "Field is null");
		}
		
		try
		{
			Double.parseDouble(this.getValue().toString());
		}catch(NumberFormatException e) {
			
			return new ValidationResult(-3, "Not a valid number");
			
		}
		
		return new ValidationResult(0, "Valid");
	}

}
