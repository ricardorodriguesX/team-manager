package ricardo.teammanager.utils.validator;

public class ValidationResult {
	
	private int code;
	
	private String message;

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
	
	public ValidationResult(int code, String message)
	{
		super();
		
		this.code = code;
		
		this.message = message;
	}
	
}
