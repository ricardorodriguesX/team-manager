package ricardo.teammanager.utils.validator;

public abstract class Validation {
	
	private Object value;
	
	protected Object getValue()
	{
		return this.value;
	}
	
	public Validation(Object value)
	{
		this.value = value;
	}
	
	public abstract ValidationResult validate();
	
}
