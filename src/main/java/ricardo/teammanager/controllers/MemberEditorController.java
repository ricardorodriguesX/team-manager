package ricardo.teammanager.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import ricardo.teammanager.App;
import ricardo.teammanager.daos.MemberDao;
import ricardo.teammanager.models.Member;
import ricardo.teammanager.utils.validator.validations.Required;

public class MemberEditorController extends Controller{
	
	@FXML
	private TextField id;
	
	@FXML
	private TextField code;
	
	@FXML
	private TextField name;
	
	private Member member;
	
	public MemberEditorController(Member member) 
	{
		this.member = member;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		super.initialize(location, resources);
		
		id.setText(String.valueOf(member.getId()));
		
		code.setText(member.getCode());
		
		name.setText(member.getName());
	}
	
	@FXML
	public void goBack(ActionEvent event) throws IOException
	{
		App.setRoot("members", null);
	}
	
	@FXML
	public void updateUser(ActionEvent event) throws IOException
	{
		this.validate(
				new Required(code.getText()),
				new Required(name.getText())
		);
		
		if(!this.isValid()) {
			return;
		}
		
		member.setCode(code.getText());
		
		member.setName(name.getText());
		
		MemberDao.update(member);
	}
	
	@FXML
	public void deleteUser(ActionEvent event) throws IOException
	{
		if(member == null) {
			return;
		}
		
		if(!member.getEntries().isEmpty()) {
			return;
		}
		
		MemberDao.delete(member);
		
		App.setRoot("members", null);
	}
}
