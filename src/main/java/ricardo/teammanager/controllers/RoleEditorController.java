package ricardo.teammanager.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import ricardo.teammanager.App;
import ricardo.teammanager.daos.RoleDao;
import ricardo.teammanager.models.Role;
import ricardo.teammanager.utils.validator.validations.Required;

public class RoleEditorController extends Controller{
	
	@FXML
	private TextField id;
	
	@FXML
	private TextField name;
	
	@FXML
	private TextArea description;
	
	private Role role;
	
	public RoleEditorController(Role role) 
	{
		this.role = role;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		super.initialize(location, resources);
		
		id.setText(String.valueOf(role.getId()));
		
		name.setText(role.getName());
		
		description.setText(role.getDescription());
	}
	
	@FXML
	public void goBack(ActionEvent event) throws IOException
	{
		App.setRoot("roles", null);
	}
	
	@FXML
	public void updateRole(ActionEvent event) throws IOException
	{
		this.validate(
				new Required(name.getText()),
				new Required(description.getText())
		);
		
		if(!this.isValid()) {
			return;
		}
		
		role.setName(name.getText());
		
		role.setDescription(description.getText());
		
		RoleDao.update(role);
	}
	
	@FXML
	public void deleteRole(ActionEvent event) throws IOException
	{
		if(role == null) {
			return;
		}
		
		if(!role.getEntries().isEmpty()) {
			return;
		}
		
		RoleDao.delete(role);
		
		App.setRoot("roles", null);
	}
}
