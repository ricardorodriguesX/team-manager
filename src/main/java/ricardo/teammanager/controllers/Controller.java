package ricardo.teammanager.controllers;

import java.io.IOException;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import ricardo.teammanager.App;
import ricardo.teammanager.utils.validator.Validation;
import ricardo.teammanager.utils.validator.ValidationResult;

public class Controller implements Initializable 
{
	private List<ValidationResult> validationResult;
	
	public Controller()
	{
		this.validationResult = new ArrayList<ValidationResult>();
	}
	
	protected void validate(Validation... validations)
	{
		for(Validation validation : validations) {
			validationResult.add(validation.validate());
		}
	}
	
	protected boolean isValid() {		
		return !validationResult.stream()
			.anyMatch(result -> result.getCode() != 0);
	}
	
	@FXML
	public void entityClick(ActionEvent event) throws IOException
	{
		MenuItem menuItem = (MenuItem) event.getTarget();
		
		App.setRoot(menuItem.getText(), null);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
}
