package ricardo.teammanager.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import ricardo.teammanager.App;
import ricardo.teammanager.daos.MemberDao;
import ricardo.teammanager.models.Member;
import ricardo.teammanager.observables.MemberObservable;

public class MembersController extends Controller {
	@FXML
	private TableView<MemberObservable> usersTable;

	@FXML
	private TableColumn<MemberObservable, Integer> colId;

	@FXML
	private TableColumn<MemberObservable, String> colCode;

	@FXML
	private TableColumn<MemberObservable, String> colName;
	
	@FXML
	private TextField code;
	
	@FXML
	private TextField name;
	
	private int offset;

	public MembersController() {
		
		this.offset = 0;
		
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		super.initialize(location, resources);

		colId.setCellValueFactory(new PropertyValueFactory<MemberObservable, Integer>("id"));

		colCode.setCellValueFactory(new PropertyValueFactory<MemberObservable, String>("code"));

		colName.setCellValueFactory(new PropertyValueFactory<MemberObservable, String>("name"));

		this.fillUsers();
	}

	@FXML
	public void openUserRegister(ActionEvent event) throws IOException {
		App.setRoot("member_register", null);
	}

	@FXML
	public void onFocus(MouseEvent event) {
		this.fillUsers();
	}

	@FXML
	private void rowClicked(MouseEvent event) throws IOException
	{
		if(event.isSecondaryButtonDown() || event.getClickCount() != 2) {
			return;
		}
		
		MemberObservable memberObservable = usersTable.getSelectionModel().getSelectedItem();
		
		if(memberObservable == null) {
			return;
		}
		
		Member member = memberObservable.toMember();
		
		MemberEditorController editorController = new MemberEditorController(member);
		
		App.setRoot("member_editor", editorController);
	}
	
	@FXML
	private void clearFilters(MouseEvent event) {
		code.setText("");
		name.setText("");
		
		this.fillUsers();
	}
	
	@FXML
	private void searchUsers(MouseEvent event) {
		this.fillUsers();
	}
	
	@FXML
	private void nextPage(ActionEvent event) {
		this.offset += 10;
		
		this.fillUsers();
		
		if(usersTable.getItems().size() == 0) {
			this.offset -= 10;
			
			this.fillUsers();
		};
	}
	
	@FXML
	private void previousPage(ActionEvent event) {
		this.offset -= 10;
		
		this.fillUsers();
		
		if(usersTable.getItems().size() == 0) {
			this.offset += 10;
			
			this.fillUsers();
		};
	}
	
	private void fillUsers() {

		ObservableList<MemberObservable> membersObservables = FXCollections.observableArrayList();
		
		MemberDao.search(code.getText(), name.getText(), this.offset, 10).forEach(member -> {
			
			membersObservables.add(new MemberObservable(member));
		});
		
		usersTable.setItems(membersObservables);
		
		usersTable.refresh();
	}
}
