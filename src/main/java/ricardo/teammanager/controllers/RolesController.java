package ricardo.teammanager.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import ricardo.teammanager.App;
import ricardo.teammanager.daos.RoleDao;
import ricardo.teammanager.models.Role;
import ricardo.teammanager.observables.RoleObservable;

public class RolesController extends Controller {
	@FXML
	private TableView<RoleObservable> rolesTable;

	@FXML
	private TableColumn<RoleObservable, Integer> colId;

	@FXML
	private TableColumn<RoleObservable, String> colName;

	@FXML
	private TableColumn<RoleObservable, String> colDescription;
	
	@FXML
	private TextField name;
	
	private int offset;

	public RolesController() {
		
		this.offset = 0;
		
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		super.initialize(location, resources);

		colId.setCellValueFactory(new PropertyValueFactory<RoleObservable, Integer>("id"));

		colName.setCellValueFactory(new PropertyValueFactory<RoleObservable, String>("name"));

		colDescription.setCellValueFactory(new PropertyValueFactory<RoleObservable, String>("description"));

		this.fillRoles();
	}

	@FXML
	public void openRoleRegister(ActionEvent event) throws IOException {
		App.setRoot("role_register", null);
	}

	@FXML
	public void onFocus(MouseEvent event) {
		this.fillRoles();
	}

	@FXML
	private void rowClicked(MouseEvent event) throws IOException
	{
		if(event.isSecondaryButtonDown() || event.getClickCount() != 2) {
			return;
		}
		
		RoleObservable roleObservable = rolesTable.getSelectionModel().getSelectedItem();
		
		if(roleObservable == null) {
			return;
		}
		
		Role role = roleObservable.toRole();
		
		RoleEditorController editorController = new RoleEditorController(role);
		
		App.setRoot("role_editor", editorController);
	}
	
	@FXML
	private void clearFilters(MouseEvent event) {
		name.setText("");
		
		this.fillRoles();
	}
	
	@FXML
	private void searchRoles(MouseEvent event) {
		this.fillRoles();
	}
	
	@FXML
	private void nextPage(ActionEvent event) {
		this.offset += 10;
		
		this.fillRoles();

		if(rolesTable.getItems().size() == 0) {
			this.offset -= 10;
			
			this.fillRoles();
		};
	}
	
	@FXML
	private void previousPage(ActionEvent event) {
		this.offset -= 10;
		
		this.fillRoles();
		
		if(rolesTable.getItems().size() == 0) {
			this.offset += 10;
			
			this.fillRoles();
		};
	}
	
	private void fillRoles() {
		
		ObservableList<RoleObservable> rolesObservables = FXCollections.observableArrayList();
		
		RoleDao.search(name.getText(), this.offset, 10).forEach(role -> {
			
			rolesObservables.add(new RoleObservable(role));
		});
		
		rolesTable.setItems(rolesObservables);
		
		rolesTable.refresh();
	}
}
