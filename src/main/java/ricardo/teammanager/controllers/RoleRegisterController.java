package ricardo.teammanager.controllers;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import ricardo.teammanager.App;
import ricardo.teammanager.daos.RoleDao;
import ricardo.teammanager.models.Role;
import ricardo.teammanager.utils.validator.validations.Required;

public class RoleRegisterController extends Controller{

	@FXML
	private TextField name;
	
	@FXML
	private TextArea description;
	
	public RoleRegisterController() 
	{

	}
	
	@FXML
	public void goBack(ActionEvent event) throws IOException
	{
		App.setRoot("roles", null);
	}
	
	@FXML
	public void createRole(ActionEvent event) throws IOException
	{
		this.validate(
				new Required(name.getText()),
				new Required(description.getText())
		);
		
		if(!this.isValid()) {
			return;
		}
		
		Role role = new Role();
		
		role.setName(name.getText());
		
		role.setDescription(description.getText());
		
		RoleDao.create(role);
	}
}
