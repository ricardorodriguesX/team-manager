package ricardo.teammanager.controllers;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import ricardo.teammanager.App;
import ricardo.teammanager.daos.MemberDao;
import ricardo.teammanager.models.Member;
import ricardo.teammanager.utils.validator.validations.Required;

public class MemberRegisterController extends Controller{

	@FXML
	private TextField code;
	
	@FXML
	private TextField name;
	
	public MemberRegisterController() 
	{

	}
	
	@FXML
	public void goBack(ActionEvent event) throws IOException
	{
		App.setRoot("members", null);
	}
	
	@FXML
	public void createUser(ActionEvent event) throws IOException
	{
		this.validate(
				new Required(code.getText()),
				new Required(name.getText())
		);
		
		if(!this.isValid()) {
			return;
		}
		
		Member member = new Member();
		
		member.setCode(code.getText());
		
		member.setName(name.getText());
		
		MemberDao.create(member);
		
		System.out.println("Member created: " + member.getId());
	}
}
