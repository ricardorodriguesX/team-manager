package ricardo.teammanager.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Entry extends Model implements Serializable
{

	private static final long serialVersionUID = 2186106825696181355L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private Date started;
	
	private Date finished;
	
	@ManyToOne(cascade=CascadeType.ALL)  
	private Member member;
	
	@ManyToOne(cascade=CascadeType.ALL)
	private Role role;

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public Date getStarted() 
	{
		return started;
	}

	public void setStarted(Date started) 
	{
		this.started = started;
	}

	public Date getFinished() 
	{
		return finished;
	}

	public void setFinished(Date finished) 
	{
		this.finished = finished;
	}

	public Member getMember() 
	{
		return member.clone();
	}

	public void setMember(Member member) 
	{
		this.member = member.clone();
	}

	public Role getRole() 
	{
		return role.clone();
	}

	public void setRole(Role role) 
	{
		this.role = role.clone();
	}

	public static long getSerialversionuid() 
	{
		return serialVersionUID;
	}
	
	public Entry()
	{
		
	}

	public Entry(int id, Date started, Date finished, Member member, Role role) 
	{
		this.id = id;
		this.started = started;
		this.finished = finished;
		this.member = member.clone();
		this.role = role.clone();
	}
	
	public Entry(Entry entry)
	{
		this.id = entry.getId();
		this.started = entry.getStarted();
		this.finished = entry.getFinished();
		this.member = entry.getMember().clone();
		this.role = entry.getRole().clone();
	}

	@Override
	public Entry clone() 
	{
		return new Entry(this.id, this.started, this.finished, this.member.clone(), this.role.clone());
	}

}
