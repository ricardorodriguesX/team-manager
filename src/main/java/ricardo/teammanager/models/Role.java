package ricardo.teammanager.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Role extends Model implements Serializable
{

	private static final long serialVersionUID = -3072026126974253898L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private int id;
	
	@Column(unique = true)
	private String name;
	
	private String description;
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private List<Entry> entries;

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	public List<Entry> getEntries() {
		return entries;
	}

	public void setEntries(List<Entry> entries) {
		this.entries = entries;
	}

	public static long getSerialversionuid() 
	{
		return serialVersionUID;
	}
	
	public Role()
	{
		
	}
	
	public Role(int id, String name, String description, List<Entry> entries) 
	{
		this.id = id;
		this.name = name;
		this.description = description;
		this.entries = entries;
	}
	
	public Role(Role function)
	{
		this.id = function.getId();
		this.name = function.getName();
		this.description = function.getDescription();
		this.entries = function.getEntries();
	}
	
	@Override
	public Role clone() 
	{
		return new Role(this.id, this.name, this.description, this.entries);
	}
}
