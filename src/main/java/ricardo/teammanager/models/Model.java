package ricardo.teammanager.models;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Model {
	
	public abstract Model clone();
	
}
