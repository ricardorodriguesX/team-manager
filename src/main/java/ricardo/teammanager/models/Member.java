package ricardo.teammanager.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Member extends Model implements Serializable{

	private static final long serialVersionUID = -6521445907215823981L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(unique = true)
	private String code;
	
	private String name;
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private List<Entry> entries;

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public String getCode() 
	{
		return code;
	}

	public void setCode(String code) 
	{
		this.code = code;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public List<Entry> getEntries() {
		return entries;
	}

	public void setEntries(List<Entry> entries) {
		this.entries = entries;
	}
	
	public static long getSerialversionuid() 
	{
		return serialVersionUID;
	}
	
	public Member() {
		
	}

	public Member(int id, String code, String name, List<Entry> entries) 
	{
		this.id = id;
		this.code = code;
		this.name = name;
		this.entries = entries;
	}
	
	public Member(Member member) 
	{
		this.id = member.id;
		this.code = member.code;
		this.name = member.name;
		this.entries = member.getEntries();
	}

	@Override
	public Member clone() 
	{
		return new Member(this.id, this.code, this.name, this.entries);
	}
	
}
