package ricardo.teammanager;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {

        scene = new Scene(loadFXML("main", null));
        stage.setScene(scene);
        stage.show();
       
    }

    public static void setRoot(String fxml, Object controller) throws IOException {
        scene.setRoot(loadFXML(fxml.toLowerCase(), controller));
    }

    private static Parent loadFXML(String fxml, Object controller) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        
        if(controller != null) {
        	fxmlLoader.setController(controller);
        }
        
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}