package ricardo.teammanager.observables;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import ricardo.teammanager.daos.MemberDao;
import ricardo.teammanager.models.Member;

public class MemberObservable {


	private IntegerProperty id;
	
	private StringProperty code;
	
	private StringProperty name;
	
	private Member member;

	public Integer getId() 
	{
		return id.getValue();
	}

	public String getCode() 
	{
		return code.getValue();
	}

	public String getName() 
	{
		return name.getValue();
	}

	public Member getMember()
	{
		return member.clone();
	}
	
	public Member toMember() {
		return MemberDao.find(this.id.getValue());
	}
	
	public MemberObservable(Member member) 
	{
		this.member = member.clone();
		
		this.id = new SimpleIntegerProperty(this.member.getId());
		
		this.code = new SimpleStringProperty(this.member.getCode());
		
		this.name = new SimpleStringProperty(this.member.getName());
	}
	
}

