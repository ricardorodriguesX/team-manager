package ricardo.teammanager.observables;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import ricardo.teammanager.daos.RoleDao;
import ricardo.teammanager.models.Role;

public class RoleObservable {

	private IntegerProperty id;
	
	private StringProperty name;
	
	private StringProperty description;
	
	private Role role;

	public Integer getId() 
	{
		return id.getValue();
	}

	public String getName() 
	{
		return name.getValue();
	}
	
	public String getDescription() 
	{
		return description.getValue();
	}

	public Role getRole()
	{
		return role.clone();
	}
	
	public Role toRole() {
		return RoleDao.find(this.id.getValue());
	}
	
	public RoleObservable(Role role) 
	{
		this.role = role.clone();
		
		this.id = new SimpleIntegerProperty(this.role.getId());
		
		this.name = new SimpleStringProperty(this.role.getName());
		
		this.description = new SimpleStringProperty(this.role.getDescription());
	}
	
}

