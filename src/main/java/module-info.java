module ricardo.teammanager {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
	requires org.hibernate.orm.core;
	requires org.hibernate.commons.annotations;
	requires java.persistence;
	requires java.sql;
	
    opens ricardo.teammanager.controllers to javafx.fxml;
    
    opens ricardo.teammanager.models to org.hibernate.orm.core, javafx.base;
    
    opens ricardo.teammanager.observables to javafx.base;
    
    exports ricardo.teammanager;
}